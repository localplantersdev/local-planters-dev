const Migrations = artifacts.require("Migrations");
const simpleERC20 = artifacts.require("SimpleStorage");

module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(simpleERC20);
  
};
